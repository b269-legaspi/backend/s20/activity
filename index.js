let inputNumber = prompt("input number here");

for (inputNumber; inputNumber >= 50; inputNumber--) {
  if (inputNumber <= 50) {
    console.log("The current value is at 50. Terminating the loop");
    break;
  }

  if (inputNumber % 5 === 0) {
    console.log(inputNumber);
  }

  if (inputNumber % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number");
  }
}

let stringNoVowel = "";
const string = "supercalifragilisticexpialidocious";

for (let i = 0; i < string.length; i++) {
  if (
    string[i] == "a" ||
    string[i] == "e" ||
    string[i] == "i" ||
    string[i] == "o" ||
    string[i] == "u"
  ) {
    continue;
  } else {
    stringNoVowel += string[i];
  }
}

console.log(stringNoVowel);

// "a" || "e" || "i" || "o" || "u"

/* string[i] != "a" ||
string[i] != "e" ||
string[i] != "i" ||
string[i] != "s" ||
string[i] != "u"
*/

// // console.log("Hello World!");

// // [SECTION] - 3 Kinds of loops
// /*
// 1. while loop
// 2. do-while loop
// 3. for loop
// */

// // While Loop
// /*
// -A while loop takes in an expression/condition
// -if the condition evaluates true, the statement inside the code will be executed.
// -A loop will iterate a certain times depending on the syntax

// SYNTAX:

// while(expression/condition){
// 	//code block or statement
// }

// */

// let count = 5;
// //4
// //3
// //2
// //1
// //0

// // The current value of count is printed out
// while(count !== 0){
// 	console.log("While: " + count);
// 	// Decreases the value of count by 1 after every iteration.
// 	count--;
// }

// // do-while loop
// /*
// - A do-while loop works like the while loop. But unlike while loop, do-while loop guarantee that the code will be executed once.

// SYNTAX:

// do{
// 	//code block - statement
// }while(expression/condition)

// */
// // Number() - is a method for converting string number to integer. "1" -> 1 = integer

// /*

// THIS IS OUR CODE, COMMENTED-OUT

// let number = Number(prompt("Give me a number: "));

// do{
// 	console.log("Do While: " + number);
// 	// Increase the value of number by 1 after every iteration.
// 	number += 1;
// }while(number <= 10);

// */

// // for loop
// /*
// - A for loop is more flexible than while and do-while loop.
// -It consist 3 parts:
// 	a. The initialization
// 	b. Condition
// 	c. Final expression/incrementation or decrementaiton

// SYNTAX:

// for(initialization; condition; finalExpression){
// 	//code block
// }

// */

// /*

// THIS IS OUR CODE FOR FORLOOPS

// for(let count = 0; count <= 20; count++){
// 	console.log("For: " + count);
// }

// */

// let myString = "alex";
// // Characters in string may be counted using the .length property.
// // A empty space is considered character

// console.log(myString.length);

// // Accessing the element of a string
// // We will be using index number
// // index number always start counting from 0

// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);

// // Create a loop that will print out the individual letters of myString variable.

// for(let x = 3; x < myString.length; x++){
// 	console.log(myString[x]);
// 	// console.log(myString[0]);
// 	// console.log(myString[1]);
// 	// console.log(myString[2]);
// 	// console.log(myString[3]);
// }

// // For loops and if-else statement

// /*
// 		    - Creates a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
// 		    - How this For Loop works:
// 		        1. The loop will start at 0 for the the value of "i"
// 		        2. It will check if "i" is less than the length of myName (e.g. 0)
// 		        3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
// 		        4. If the expression/condition is true the console will print the number 3.
// 		        5. If the letter is not a vowel the console will print the letter
// 		        6. The value of "i" will be incremented by 1 (e.g. i = 1)
// 		        7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
// */

// let myName = "Romenick Garcia";

// for(let i = 0; i < myName.length; i++){

// 	// If the character of your name is a vowel letter, insted of displaying character, display 3
// 	// The .toLowerCase() will froce string to go lower case.

// 	if(
// 		myName[i].toLowerCase() == "a" ||
// 		myName[i].toLowerCase() == "e" ||
// 		myName[i].toLowerCase() == "i" ||
// 		myName[i].toLowerCase() == "o" ||
// 		myName[i].toLowerCase() == "u"

// 		){
// 		console.log(3);
// 	}else{
// 		console.log(myName[i]);
// 	}

// }

// // [Section] Continue and Break statements
// // Initializing a for loop that will start at zero (0) and will iterate until the condition is no longer met.
// for(let count = 0; count <= 20; count++){

// 	// Checking if there is no remainder for the count. The 'continue' expression will basically continue the loop.
// 	if(count % 2 === 0) {
// 		continue;
// 	}

// 	console.log("Continue and Break" + count);

// 	// Checks if the current count is less than 10. If it is, then break the whole loop entirely.
// 	if(count > 10){
// 		break;
// 	}
// }

// let name = "alexandro";

// // Initializing a loop which will continue iterating as long as the index is less than the length of the 'name' string variable.
// for(let index = 0; index < name.length; index++){
// 	// Prints the current letter based on the current index
// 	console.log(name[index]);

// 	if(name[index].toLowerCase() === "a"){
// 		console.log("Continue to the next iteration")
// 		continue;
// 	}

// 	if(name[index] == "d"){
// 		break;
// 	}
// }
